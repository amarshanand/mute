## How would it work

1. We would set our consuming React App (call it cApp) to start with a default theme (could be no theme, ie, MUI default)
2. At bootstrap, cApp will look for Mute (or perhaps Mute will look for cApp)
3. During handshake, cApp will push its current theme to Mute as a starting point
4. cApp will listen to any messages from Mute, and will update its Theme accordingly
