import React, { useState } from "react";
import {
  ButtonGroup,
  Button,
  Snackbar,
  Alert,
  createTheme,
  ThemeProvider,
} from "@mui/material";

interface ControlsProps {
  showThemeEditor: boolean;
  setShowThemeEditor: (show: boolean) => void;
  useHostTheme: boolean;
  theme: any;
  setUseHostTheme: (use: boolean) => void;
}

const _theme = createTheme({
  palette: {
    primary: {
      main: "#191c24",
    },
    text: {
      primary: "#72f333",
    },
  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          fontSize: "0.8rem",
          textTransform: "none",
          fontWeight: 500,
        },
      },
    },
  },
});

const Controls: React.FC<ControlsProps> = ({
  showThemeEditor,
  setShowThemeEditor,
  useHostTheme,
  theme,
  setUseHostTheme,
}) => {
  const [openSnackbar, setOpenSnackbar] = useState(false);

  const handleCopyTheme = () => {
    navigator.clipboard
      .writeText(JSON.stringify(theme, null, 2))
      .then(() => {
        setOpenSnackbar(true);
      })
      .catch((err) => {
        console.error("Failed to copy theme: ", err);
      });
  };

  const handleCloseSnackbar = (
    event: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenSnackbar(false);
  };

  return (
    <ThemeProvider theme={_theme}>
      <ButtonGroup
        variant="contained"
        aria-label="outlined primary button group"
        sx={{
          position: "fixed",
          bottom: "2px",
          width: "100%",
          "& .MuiButton-root": {
            borderRadius: 0,
          },
        }}
        fullWidth
      >
        <Button
          onClick={() => setShowThemeEditor(!showThemeEditor)}
          sx={{ flexGrow: 1, textTransform: "none", color: "text.primary" }}
          style={{ borderTopLeftRadius: showThemeEditor ? 0 : "4px" }}
        >
          {showThemeEditor ? "Close Editor" : "Edit Theme"}
        </Button>
        <Button
          onClick={handleCopyTheme}
          sx={{ flexGrow: 1, textTransform: "none", color: "text.primary" }}
        >
          Copy Theme
        </Button>
        <Button
          onClick={() => setUseHostTheme(!useHostTheme)}
          sx={{ flexGrow: 1, textTransform: "none", color: "text.primary" }}
          style={{ borderTopRightRadius: showThemeEditor ? 0 : "4px" }}
        >
          {useHostTheme ? "MUI Default" : "Apply Theme"}
        </Button>
      </ButtonGroup>
      <Snackbar
        open={openSnackbar}
        autoHideDuration={3000}
        onClose={handleCloseSnackbar}
      >
        <Alert
          onClose={handleCloseSnackbar}
          severity="success"
          sx={{ width: "100%" }}
        >
          Theme copied to clipboard
        </Alert>
      </Snackbar>
    </ThemeProvider>
  );
};

export default Controls;
