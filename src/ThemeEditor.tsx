import React, { useEffect, useState } from "react";
import { JsonEditor as Editor } from "jsoneditor-react18";
import "jsoneditor-react18/es/editor.min.css";
import styled from "styled-components";

interface ThemeEditorProps {
  theme: object;
  onThemeChange: (newTheme: object) => void;
}

const StyledJsonEditorWrapper = styled.div`
  .jsoneditor {
    border: none;
  }
  .jsoneditor-tree {
    max-height: 100vh;
    overflow: scroll;
  }
  .jsoneditor-tree-inner {
    margin-bottom: 8rem;
  }
  .jsoneditor-menu {
    background-color: #191c24;
    border-bottom: 1px solid #191c24;
  }
`;

const ThemeEditor: React.FC<ThemeEditorProps> = ({ theme, onThemeChange }) => {
  const [, setEditorContent] = useState(theme);
  useEffect(() => {
    setEditorContent(theme);
  }, [theme]);

  const handleChange = (newContent: object) => {
    setEditorContent(newContent);
    onThemeChange(newContent);
  };

  return (
    <StyledJsonEditorWrapper>
      <div
        style={{
          maxHeight: "calc(100vh - 2.5rem)",
          minHeight: "calc(100vh - 2.5rem)",
          overflow: "hidden",
          border: "2px solid #191c24",
          background: "white",
        }}
      >
        <Editor schema={{}} value={theme} onChange={handleChange} />
      </div>
    </StyledJsonEditorWrapper>
  );
};

export default ThemeEditor;
