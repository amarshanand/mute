import { useState, useEffect } from "react";
import ThemeEditor from "./ThemeEditor";
import Controls from "./Controls";
import { Typography, Alert } from "@mui/material";

const alertStyle: React.CSSProperties = {
  width: "100%",
  margin: 0,
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  border: "2px solid #f44336",
  maxWidth: "364px",
  position: "fixed",
  bottom: 0,
};

function App() {
  const [currentTheme, setCurrentTheme] = useState<object | null>(null);
  const [useHostTheme, setUseHostTheme] = useState(false);
  const [showThemeEditor, setShowThemeEditor] = useState(false);

  useEffect(() => {
    function handleReceiveMessage(event: MessageEvent) {
      const data = event.data as {
        hostTheme?: object;
      };
      if (data.hostTheme) {
        setCurrentTheme(data.hostTheme);
        setUseHostTheme(true);
      }
    }
    window.addEventListener("message", handleReceiveMessage);
    return () => window.removeEventListener("message", handleReceiveMessage);
  }, []);

  if (!currentTheme) {
    return (
      <Alert severity="warning" style={alertStyle}>
        <Typography variant="body1" style={{ textAlign: "center" }}>
          Waiting to connect ...
        </Typography>
      </Alert>
    );
  }

  return (
    <>
      {currentTheme && showThemeEditor && (
        <ThemeEditor
          theme={currentTheme}
          onThemeChange={(newTheme) =>
            window.parent.postMessage({ newTheme }, "*")
          }
        />
      )}
      <Controls
        showThemeEditor={showThemeEditor}
        setShowThemeEditor={setShowThemeEditor}
        setUseHostTheme={(isUseHostTheme) => {
          setUseHostTheme(isUseHostTheme);
          window.parent.postMessage(
            isUseHostTheme ? "hostTheme" : "noTheme",
            "*"
          );
        }}
        theme={currentTheme}
        useHostTheme={useHostTheme}
      />
    </>
  );
}

export default App;
