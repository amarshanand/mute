import React, { ReactNode, useState, useEffect, useRef, useMemo } from "react";
import { ThemeProvider, createTheme, ThemeOptions } from "@mui/material/styles";

interface MUIThemeEditorProps {
  children: ReactNode;
  themeJson: ThemeOptions;
}

const iframeStyle: React.CSSProperties = {
  width: "400px",
  height: "400px",
  border: "none",
  borderRadius: "4px",
  position: "fixed",
  bottom: "0.5rem",
  left: "0.5rem",
  zIndex: Number.MAX_SAFE_INTEGER,
};

const MUIThemeEditor: React.FC<MUIThemeEditorProps> = ({
  children,
  themeJson,
}) => {
  const [theme, setTheme] = useState<ThemeOptions>(themeJson);
  const [isThemingEnabled, setIsThemingEnabled] = useState<boolean>(true);
  const iframeRef = useRef<HTMLIFrameElement>(null);
  const [isUrlAvailable, setIsUrlAvailable] = useState(false);

  const themeEditorURL =
    (process && process.env?.REACT_APP_THEME_EDITOR_URL) ||
    "https://mute-amarshanand-6b265cf756659b65d41cd645b6f87e8f8ccebd009935eb.gitlab.io/";
  if (!themeEditorURL || themeEditorURL === "") {
    console.error(
      "REACT_APP_THEME_EDITOR_URL is not set in env vars. Theme editor will not be available."
    );
  }

  // Check if the theme editor URL is reachable
  useEffect(() => {
    if (themeEditorURL) {
      fetch(themeEditorURL, { method: "HEAD" })
        .then((response) => {
          if (response.ok) {
            setIsUrlAvailable(true);
          } else {
            console.error(
              `The theme editor URL ${themeEditorURL} is not reachable`
            );
          }
        })
        .catch(() => {
          console.error(
            `Failed to fetch the theme editor URL ${themeEditorURL}`
          );
          setIsUrlAvailable(false);
        });
    }
  }, [themeEditorURL]);

  // Listen for messages from the themeEditor iframe
  useEffect(() => {
    const handleReceiveMessage = (event: MessageEvent) => {
      if (event.data === "hostTheme") {
        setIsThemingEnabled(true);
      } else if (event.data === "noTheme") {
        setIsThemingEnabled(false);
      } else if (event.data.newTheme) {
        if (isThemingEnabled) {
          setTheme(event.data.newTheme);
        }
      }
    };

    themeEditorURL && window.addEventListener("message", handleReceiveMessage);

    if (isUrlAvailable) {
      setTimeout(() => {
        iframeRef.current?.contentWindow?.postMessage(
          { hostTheme: theme },
          themeEditorURL
        );
      }, 1000);
    }

    return () => window.removeEventListener("message", handleReceiveMessage);
  }, [isUrlAvailable]);

  const appliedTheme = createTheme(isThemingEnabled ? theme : {});

  const EditorIframe = useMemo(
    () => (
      <iframe
        ref={iframeRef}
        src={themeEditorURL}
        style={iframeStyle}
        allow="clipboard-read; clipboard-write"
        title="Theme Editor"
      ></iframe>
    ),
    [themeEditorURL]
  );

  return (
    <>
      <ThemeProvider theme={appliedTheme}>
        {children}
        {themeEditorURL && isUrlAvailable && EditorIframe}
      </ThemeProvider>
    </>
  );
};

export default MUIThemeEditor;
